<?php 

require_once __DIR__.'/../vendor/Myframe/Autoloader.php';
$autoloader = new Autoloader();

$autoloader->addNamespace('Myframe', __DIR__.'/../vendor/Myframe/');
$autoloader->addNamespace('App', __DIR__.'/../app/');
$autoloader->register();

use Myframe\Classes\AppKernel;

$kernel = new AppKernel();
$kernel->handle($_REQUEST);

