
<?php if (!$is_ajax) { include_once '/../header.php'; } ?>

<div id="news_list">
	<h1><?php echo $conf['sitename']; ?></h1>
	
	<?php 
		foreach ($News as $N) {
			echo "<div>";
			echo $N->getLabel();
			echo "</div>";
		}
	?>

	<div class="pager">
		<?php
			if ($pager['count'] > 1) { 			
				for ($i=1; $i<=$pager['count']; $i++) {
					if ($pager['page'] == $i) {
						echo "<span>$i</span>";
					} else {
						echo "<a href='/news/list/$i/' class='page_a'>$i</a>";
					}
				}
			}
		?>				
	</div>	
	
	<script>
		$("a.page_a").each(function(index) {
			$(this).on("click", function(){
				var url = $(this).attr('href');
				
				$.ajax({
					url: url,
					success: function(data) {
						$('#news_list').html(data);
					}
				});
				
				return false; 
			});
		});	
	</script>	
</div>	
	
<?php if (!$is_ajax) { include_once '/../footer.php'; } ?>