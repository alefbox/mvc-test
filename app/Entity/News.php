<?php

namespace App\Entity;

use App\Entity\BaseEntity;

class News extends BaseEntity
{
	protected $id;
	protected $label;
	protected $name;
	protected $text;
	protected $created_at ;
	
	public function getId()
	{
		return $this->id;
	}
	
	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}
	
	public function getLabel()
	{
		return $this->label;	
	}
	
	public function setLabel($label)
	{
		$this->label = $label;
		return $this;
	}

	public function getName()
	{
		return $this->name;	
	}
	
	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}

	public function getText()
	{
		return $this->text;	
	}
	
	public function setText($text)
	{
		$this->text = $text;
		return $this;
	}

	public function getCreatedAt()
	{
		return $this->created_at;	
	}
	
	public function setCreatedAt($created_at)
	{
		$this->created_at = $created_at;
		return $this;
	}
	
}
