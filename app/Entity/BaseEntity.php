<?php

namespace App\Entity;

class BaseEntity
{
    public function fromArray($array)
    {
    	foreach($array as $name => $value)
    	{
    		$names = explode('_',$name);
    		$name = 'set';
    		foreach ($names as $part) {
    			$name .= ucfirst($part);
    		}
    		if (method_exists($this, $name)) {
    			call_user_func(array($this, $name), $value);
    		}
    	}
    	return $this;
    }
}
