<?php

use Myframe\Doctrine\Repository;
use App\Entity\News;

class NewsRepository extends Repository 
{
	public function getAll($limit='', $offset='')
	{
		$stmt = $this->pdo->prepare("SELECT * FROM news LIMIT $limit OFFSET $offset");
		$stmt->execute();
		
		$News = [];
		foreach ($stmt as $row) {
			$N = new News();	
			$News[] = $N->fromArray($row);
		}
		
		return $News;
	}
	
	public function find($id)
	{
		$stmt = $this->pdo->prepare('SELECT * FROM news WHERE id = :id');
		$stmt->execute(array('id' => $id));
				
		$News = new News;
		$News = $News->fromArray($stmt->fetchAll()[0]);
		
		return $News;
	}
	
	public function findBy($id)
	{
		;
	}

	public function getRowsCount()
	{
		$stmt = $this->pdo->query('SELECT count(id) FROM news ');
		$data = $stmt->fetchAll(PDO::FETCH_NUM);
				
		return $data[0][0];
	}	
}

?>