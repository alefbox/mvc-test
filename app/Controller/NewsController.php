<?php

use Myframe\Classes\Controller;
use App\Entity\News;

class NewsController extends Controller 
{
	public function indexAction($params)
	{	
		$this->redirect('/news/list/');
	}
	
	public function listAction($params)
	{	
		if (isset($params['page'])) $page = $params['page'];
		else $page = 1;
		
		$pager = array(
			'all' => 0,
			'page' => $page,
			'max_result' => 3,
			'count' => 0
		);

		$em = $this->em;		
		$NewsRepo = $em->getRepository('NewsRepository');
		
		$pager['all'] = $NewsRepo->getRowsCount();
		$pager['count'] = ceil($pager['all']/$pager['max_result']);									
		$limit = $pager['max_result'];
		$offset = ($pager['page']-1)*$pager['max_result'];
		
		$News = $NewsRepo->getAll($limit, $offset);

		$this->render('News/list.php', array(
			'News' => $News, 
			'pager' => $pager, 
			'is_ajax' => $this->isAjax(),
			'conf' => $this->config,
		));
	}
}

?>