<?php

namespace Myframe\Classes;

class AppKernel 
{
	protected $router;
	protected $controller;
	protected $config;
	
	public function __construct() 
	{
		$this->config = include __DIR__.'/../../../config/config.php';
		$this->router = new Router();		
	}
	
	public function handle($request) 
	{
		if (!isset($request) || !$request) $request['route'] = 'index';
		$this->router->parse($request['route']);
		
		$controllerName = $this->router->controller;
		$controllerAction = $this->router->action.'Action';

		require_once __DIR__ .'/../../../app/Controller/'.$controllerName.'.php';
		
		$this->controller = new $controllerName;
		$this->controller->setConfig($this->config);
		$this->controller->__call($this->router->action.'Action', array($this->router->params));
	}
}