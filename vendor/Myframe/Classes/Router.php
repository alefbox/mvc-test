<?php

namespace Myframe\Classes;

class Router {
	protected $routes;
	
	public $controller;
	public $action;
	public $params;
	
	public function __construct() 
	{
		$this->routes = include __DIR__.'/../../../config/routing.php';
	}
	
	public function parse($route) 
	{
		foreach ($this->routes as $regxp=>$keys) {			
			$pattern = "/^".str_replace('/', '\/', $regxp)."/";
			
			if (preg_match($pattern, $route, $res)) {
				$this->controller = $keys['controller'];
				$this->action = $keys['action'];

				if (isset($keys['params'])) {
					$this->params = array($keys['params'] => $res[1]);
				}				
				break;
			}
		}		
	}
}