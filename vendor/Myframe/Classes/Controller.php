<?php

namespace Myframe\Classes;

use Myframe\Doctrine\EntityManager;

class Controller
{
	public $viewPath = '';
	public $em;
	public $config;
	
	public function __construct()
	{
		$this->viewPath = __DIR__.'/../../../app/View/';
		$this->em = new EntityManager;
	}
	
	public function setConfig($config)
	{
		$this->config = $config;
	}
	
	public function __call($methodName, $args=array())
	{
		if (method_exists($this, $methodName)) {
			return call_user_func_array(array($this, $methodName), $args);
		} else {
			throw new \Exception('In controller '.get_called_class().' method '.$methodName.' not found!');
		}	
	}
	
	public function render($viewName, $vars)
	{
		extract($vars);
		$fullpath = $this->viewPath.$viewName;
		
		if (file_exists($fullpath)) {
			ob_start();
			include $fullpath;
			echo ob_get_clean();
		} else {	
			throw new \Exception('File '.$fullpath.' not found');		
		}	
	}
	
	public function redirect($url)
	{
		header("Location: $url", true, 301 );
	}
	
	public function isAjax()
	{
		if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			return true;
		}
		
		return false;
	}
}