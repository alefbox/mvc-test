<?php

namespace Myframe\Doctrine;

class EntityManager
{
	protected $config;
	protected $pdo;
	
	public function __construct()
	{
		$this->config = include __DIR__.'/../../../config/config.php';
		$this->config = $this->config['db'];
	}
	
	protected function connect()
	{
		$dsn = $this->config['db_driver'].":host=".$this->config['db_host'].";dbname=".$this->config['db_name'];		
		$this->pdo = new \PDO($dsn, $this->config['db_user'], $this->config['db_password']);
		return $this->pdo;
	}
	
	public function getRepository($repoName)
	{
		$fullpath = __DIR__ .'/../../../app/Repository/'.$repoName.'.php';
		
		if (file_exists($fullpath)) {
			require_once $fullpath;		
			$Repo = new $repoName($this->connect());
		
			return $Repo;
		} else {
			throw new \Exception('File '.$fullpath.' not found');	
		}
	}

	public function createQuery($sql)
	{
		$stmt = $this->pdo->prepare($sql);
		$stmt->execute();
		
		return $stmt->fetchAll();
	}
}
