<?php

namespace Myframe\Doctrine;

abstract class Repository
{
	protected $pdo;
	
	public function __construct($pdo) 
	{
		$this->pdo = $pdo;
	}
	
	abstract public function find($id);
	
	abstract public function findBy($id);	
	
	abstract public function getAll($limit='', $offset='');
}
