<?php
return array(
	'index'					=> array('controller' => 'NewsController', 'action' => 'index'),
	
	'news/list/([0-9]+)' 	=> array('controller' => 'NewsController', 'action' => 'list', 'params' => 'page'),
	'news/list' 			=> array('controller' => 'NewsController', 'action' => 'list'),	
);